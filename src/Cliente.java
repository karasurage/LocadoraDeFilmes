// Classe Cliente
package locadorafilmes;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * TODO: @author Nicholas Mateus
 */
public class Cliente {

	// Declaração de Variáveis
	private String nome;
	private String cpf;
	private String endereco;
	private String telefone;

	// Criação do Construtor da Classe
	public Cliente() {
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
		this.telefone = telefone;
	}

	Scanner entrada = new Scanner(System.in);

	public ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();

	// Métodos da Classe Cliente

	// Método de Cadastrar Clientes
	public void cadastraCliente() {

		Cliente cliente = new Cliente();

		System.out.println("***** CADASTRAR CLIENTE *****");

		System.out.print("Nome: ");
		cliente.setNome(entrada.nextLine());

		System.out.println("");

		System.out.print("CPF: ");
		cliente.setCpf(entrada.nextLine());

		System.out.println("");

		System.out.print("Endereço: ");
		cliente.setEndereco(entrada.nextLine());

		System.out.println("");

		System.out.print("Telefone: ");
		cliente.setTelefone(entrada.nextLine());

		System.out.println("");

		listaCliente.add(cliente);

	}

	// Método de Pesquisa de Clientes
	public void pesquisaCliente() {

		String cpf;
		boolean resultado;

		System.out.println("Digite o CPF: ");

		cpf = entrada.nextLine();

		for (Cliente cliente : listaCliente) {
			if (cliente.getCpf().equals(cpf)) {
				System.out.println("Cliente: "
									+ cliente.getNome()
									+ " | "
									+ "CPF: "
									+ cliente.getCpf());
				return;
			}
		}
		System.out.println("Cliente não encontrado");
	}

	// Méotodo de Excluir Clientes
	public void excluiCliente() {

		String cpf;
		String decisao = null;

		System.out.println("Digite o CPF: ");
		cpf = entrada.nextLine();

		for (Cliente cliente : listaCliente) {
			if (cliente.getCpf().equals(cpf)) {
				System.out.println("Cliente: "
									+ cliente.getNome()
									+ " | "
									+ "CPF: "
									+ cliente.getCpf());
			}
		}

		System.out.println("Deseja realmente excluir esse cliente? ");
		decisao = entrada.nextLine();

		if (decisao == "s") {
			listaCliente.remove(cpf);
		} else {
			return;
		}

		return;
	}

	// Getters e Setters
	public Scanner getEntrada() {
		return entrada;
	}

	public void setEntrada(Scanner entrada) {
		this.entrada = entrada;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}