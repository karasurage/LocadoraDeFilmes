// Classe LocadoraFilmes
package locadorafilmes;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import locadorafilmes.Conta;
import locadorafilmes.Filme;

/**
 * TODO: @author Nicholas Mateus
 */
public class LocadoraFilmes {

	
	/* ##### Classe Main ####
	 * Essa classe fará a execução das outras classes e métodos utilizados para esse
	 * sistema de Locadora de Filmes
	 */
	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);
		Cliente cliente = new Cliente();
		Filme filme = new Filme();

		int menu = 0;
		int opcao = 0;

		do {
			System.out.println("***** LOCADORA DE FILMES *****");
			System.out.println("1 - Cadastrar Cliente");
			System.out.println("2 - Pesquisar Cliente");
			System.out.println("3 - Excluir Cliente");
			System.out.println("4 - Cadastrar Filme");
			System.out.println("5 - Pesquisar Filme");
			System.out.println("6 - Locar Filme");
			System.out.println("7 - Histórico de Filmes");

			opcao = entrada.nextInt();

			switch (opcao) {

			case 1:
				cliente.cadastraCliente();
				break;

			case 2:
				cliente.pesquisaCliente();
				break;

			case 3:
				cliente.excluiCliente();
				break;

			case 4:
				filme.cadastraFilme();
				break;

			case 5:
				;
				filme.pesquisaFilme();
				break;

			case 6:
				;
				filme.locaFilme();
				break;

			case 7:
				;
				filme.locaFilme();
				break;

			default:
				System.out.println("Opção Inválida");
				break;
			}
		} while (menu == 0);
	}

}
