// Classe Filme
package locadorafilmes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * TODO: @author Nicholas Mateus
 */
public class Filme {

	Scanner entrada = new Scanner(System.in);

	List<Filme> listaFilme = new ArrayList<Filme>();

	// Declaração de Variáveis
	private String titulo;
	private int ano;
	private String genero;
	private double valorLocacao;

	// Criação do Construtor da Classe
	public Filme() {

		this.titulo = titulo;
		this.ano = ano;
		this.genero = genero;
		this.valorLocacao = valorLocacao;
	}

	// Métodos da Classe Filme

	// Método de Cadastrar Filme
	public void cadastraFilme() {
		Filme filme = new Filme();
		System.out.println("***** CADASTRAR FILME *****");

		System.out.print("Título: ");
		filme.setTitulo(entrada.nextLine());

		System.out.println("");

		System.out.println("Gênero: ");
		filme.setGenero(entrada.nextLine());

		System.out.println("");

		System.out.println("Ano: ");
		filme.ano = Integer.parseInt(entrada.nextLine());

		System.out.println("");

		System.out.println("Valor da Locação: ");
		filme.valorLocacao = Double.parseDouble(entrada.nextLine());

		System.out.println("");

		// Método de Listar Filmes Cadastrados no Sistema
		listaFilme.add(filme);
	}

	// Método de Pesquisar Filmes
	public void pesquisaFilme() {

		String titulo;
		boolean resultado;

		System.out.println("Digite o nome do Filme: ");

		titulo = entrada.nextLine();

		for (Filme filme : listaFilme) {
			if (filme.getTitulo().equals(titulo)) {
				System.out.println("Nome: " + filme.getTitulo() + " | " + "Gênero: " + filme.getGenero());
				return;
			}
		}
		System.out.println("Filme não encontrado");
	}

	public void locaFilme() {

		String nomeFilme;

		Cliente cliente = new Cliente();

		System.out.println("***** LOCAR FILMES *****");
		System.out.println("");
		System.out.println("Digite o nome do filme desejado: ");
		nomeFilme = entrada.nextLine();

		for (Filme filme : listaFilme) {
			if (filme.getTitulo().equals(nomeFilme)) {
				System.out.println("##### A Pesquisa Encontrou #####" 
									+ "\n Título: " + getTitulo() + "\n Ano: "
									+ getAno()
									+ "\n Gênero: "
									+ getGenero()
									+ "\n Valor: "
									+ getValorLocacao());
			}
		}
		System.out.println("Filme não encontrado");

		if (!("Filme não encontrado".equals(nomeFilme))) {

			System.out.println("Deseja realmente locar este filme? ");
			String opcao = null;

			switch (opcao) {
			case "s":
			case "S":
				System.out.println("Digite o CPF do cliente: ");
				String cpfCliente = entrada.nextLine();

				for (Cliente pcliente : cliente.listaCliente) {
					if (cliente.getCpf().equals(cliente.getCpf())) {
						System.out.println("##### A Pesquisa Encontrou #####"
											+ "\n Nome: " + cliente.getNome()
											+ "\n CPF: " + cliente.getCpf()
											+ "\n Endereço: "
											+ cliente.getEndereco()
											+ "\n Telefone: "
											+ cliente.getTelefone());
					}
				}
				System.out.println("Cliente não encontrado");

				if (!("Cliente não encontrado".equals(cpfCliente))) {

				}

				break;

			case "n":
			case "N":

			default:
				System.out.println("Aperte apenas S para SIM ou N para NÃO!");
				break;
			}

		}
	}

	// Getters e Setters
	public void setListaFilme(List<Filme> listaFilme) {
		this.listaFilme = listaFilme;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public double getValorLocacao() {
		return valorLocacao;
	}

	public void setValorLocacao(double valorLocacao) {
		this.valorLocacao = valorLocacao;
	}
}