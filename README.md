# LocadoraDeFilmes
Exercício de uma Locadora de Filmes para a disciplina de Programação Web Avançada
<hr />
<h2 align="center">.::Exercício Java – UNISAOMIGUEL/Recife::.</h2>
<p align="justify">Disciplina: Programação Orientada a Objeto</p>
<p align="justify">Professor: Danilo Farias</p>
<p align="justify">Nota: Valem 1 pontos na nota de participação(ponto extra da primeira unidade, somada a prova)</p>
<p align="justify">1) Com os aprendizados em sala de aula iremos neste exercício montar um sistema para locação de filmes. Esse sistema será baseado nas características aprendidas no nosso sistema bancário elaborado a cada aula.</p>
<p align="justify">a) Crie um projeto Java com o nome Sistemas de Locadora de Filme.</p>
<p align="justify">b) O sistema da locadora deve ter as classes representadas no diagrama de classes abaixo e os seus determinados pacotes. Criem além dos construtores descritos em cada classe, os gets e sets de cada atributo das classes, lembrem-se de criar gets e sets apenas dos atributos realmente necessários.</p>
<p align="justify">*Nos construtores de Conta já atribuir o valor 0 ao saldoDevedor;</p>
<p align="justify">c) Com as classes criadas iremos agora criar alguns métodos importantes além dos construtores, gets e sets de cada classe.
<p align="justify">- Na classe Conta iremos criar os métodos de:</p>
<p align="justify">+locarFilmes(Filmes[10] filmes):void; //Nesta classe teremos que criar uma variável local do tipo Locacao onde iremos instancia-la passando a data da locação e os filmes que serão locados um a um pelo método da classe locacao addFilme(Filme filme), por fim alocar essa locação no atributo historicoLocacao.</p>
<p align="justify">+extratoHistorico():String; //Irá retornar a String com o histórico de locações do cliente, seguindo o modelo abaixo:</p>
<h4 align="center">.:: Histórico de Locações de Nome_Cliente::.</h4>
<p align="justify">Data da locação Lista de filmes Valor da locação</p>
<p align="justify">Por Exemplo:</>
<h4 align="center">.:: Histórico de Locações de Danilo Farias::.</h4>
<p align="justify">Mon Apr 28 19:32:41 GMT-03:00 2012 Salt, Heroes 7.0</p>
<p align="justify">Mon Apr 30 19:30:43 GMT-03:00 2012 X-Men_Frist_Class 5.0</p>
<p align="justify">+ pagarDebito(double valor):void; //Desconta o valor do saldo devedor.</p>
<p align="justify">- Na classe Locacao iremos criar os métodos de:</p>
<p align="justify">+ addFilme(Filme filme):void; //Iremos adicionar o filme no atributo listaFilmes e incrementar o tributo valorTotalApagar com o valor de locação de cada filme.</p>
<p align="justify">d) Criar uma classe Principal, que será a execução do sistema. Nela deve ter ao menos 3 a 5 clientes, com suas respectivas contas, nessas contas temos 4 a 6 locações por cliente, em que cada locação seja de 1 a 5 filmes (Crie ao menos 10 filmes para servi as locações). Por fim imprima o extrato de locação de cada cliente do sistema.</p>
